# riscv-gcc-custom

Ce répertoire contient les fichiers sources de la [toolchain](https://github.com/riscv/riscv-gnu-toolchain) modifiés pour ajouter de nouvelles instructions et CSRs.
Il doivent être copié dans une copie de la toolchain pour remplacer les anciennes versions de ces fichiers.

### Modification de la toolchain (manuelle)

1. Récupérer la [toolchain](https://github.com/riscv/riscv-gnu-toolchain) RISC-V:

 ```
 git clone https://github.com/riscv/riscv-gnu-toolchain.git
 cd riscv-gnu-toolchain
 git submodule update --init --recursive
 ```

2. Copier et remplacer les fichiers (dome support):

```
cp src/dome/binutils/riscv-opc.h ../riscv-gnu-toolchain/riscv-binutils/include/opcode/riscv-opc.h
cp src/dome/binutils/riscv-opc.c ../riscv-gnu-toolchain/riscv-binutils/opcodes/riscv-opc.c
cp src/dome/Makefile.in ../riscv-gnu-toolchain/Makefile.in
```

3. Recompiler la nouvelle toolchain (voir [pré-requis](https://github.com/riscv/riscv-gnu-toolchain#prerequisites)):

```
./configure --prefix=/opt/riscv32-custom --with-arch=rv32im --with-abi=ilp32
make
```

### Liste des modifications

#### Nouveaux CSRs

- *rmupc*: adresse de lancement d'un hart (sans HSC)
- *hsccurid*: ID du HSC courant
- *hsccurpc*: point d'entrée du HSC courant
- *hsccurcap*: capacités du HSC courant
- *hscworkid*: ID du prochain HSC
- *hscworkpc*: point d'entrée du prochain HSC
- *hscworkcap*: capacités du prochain HSC

#### Nouvelles instructions

- *SHART rd*: lancer un hart statiquement (sans HSC)
- *CHART rd*: arrêter un hart statiquement (sans HSC)
- *HSCSTART rd*: lancer un hart statiquement (avec HSC)
- *HSCEND rd*: arrêter un hart statiquement (avec HSC)
- *HSCSWITCH rd*: changer le HSC d'un hart
